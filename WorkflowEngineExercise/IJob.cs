﻿namespace WorkflowEngineExercise
{
    public interface IJob
    {
        void Execute();
    }
}
