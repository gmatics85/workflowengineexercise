﻿using System.Collections.Generic;

namespace WorkflowEngineExercise
{
    public class Workflow : IWorkflow
    {
        private readonly List<IJob> _jobs;

        public Workflow()
        {
            _jobs = new List<IJob>();
        }

        public void Add(IJob job)
        {
            _jobs.Add(job);
        }

        public void Remove(IJob job)
        {
            _jobs.Remove(job);
        }

        public IEnumerable<IJob> GetJobs()
        {
            return _jobs;
        }
    }
}
