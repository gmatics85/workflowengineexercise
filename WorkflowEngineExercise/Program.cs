﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkflowEngineExercise
{
    class Program 
    {
        static void Main(string[] args)
        {
            var workflow = new Workflow();

            workflow.Add(new UpdateDatabase());
            workflow.Add(new DownloadBlacklist());
            workflow.Add(new CreateGDSFile());
            workflow.Add(new ImportGDSFile());

            var engine = new WorkflowEngine();
            engine.Run(workflow);

            Console.ReadLine();
        }
    }

    public class UpdateDatabase : IJob
    {
        public void Execute()
        {
            Console.WriteLine("Updating database...");
        }
    }

    public class DownloadBlacklist : IJob
    {
        public void Execute()
        {
            Console.WriteLine("Downloading Blacklist from API...");
        }
    }

    public class CreateGDSFile : IJob
    {
        public void Execute()
        {
            Console.WriteLine("Creating GDS file...");
        }
    }

    public class ImportGDSFile : IJob
    {
        public void Execute()
        {
            Console.WriteLine("Importing GDS file...");
        }
    }
}
