﻿using System;

namespace WorkflowEngineExercise
{
    public class WorkflowEngine
    {
        public void Run(IWorkflow workflow)
        {
            foreach (IJob job in workflow.GetJobs())
            {
                try
                {
                    // Change job status to started

                    // Run job
                    job.Execute();
                }
                catch (Exception)
                {
                    // Stop job status with Error

                    // Throw exception
                    throw;
                }
                
            }
        }
    }
}
