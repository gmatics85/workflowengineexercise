﻿using System.Collections.Generic;

namespace WorkflowEngineExercise
{
    public interface IWorkflow
    {
        void Add(IJob job);
        void Remove(IJob job);
        IEnumerable<IJob> GetJobs();
    }
}
